let tentativasRestantes = 6;
let numSorteado = 0;
let numErros = [];

const querySelector = (query = '', msg = '', type = "innerHTML", className = 'd-none') => {
    if(type == 'innerHTML') document.querySelector(query)[type] = msg;
    else document.querySelector(query)[type].toggle(className)
}

const tentativas = (restante) => querySelector('#saidaChance h4', restante);

window.addEventListener('load', function () {
    numSorteado = Math.floor(Math.random() * (60 - 1) + 1);
    tentativas(tentativasRestantes);
    console.log(`numSorteado: ${numSorteado}`);
});

setInterval(() => {
    animateCSS('#apostar', 'flipInX'); //botao
    animateCSS('.fa-heartbeat', 'swing'); //icone dos erros
    animateCSS('.fa-exclamation-circle', 'bounce'); //icone das chances
    animateCSS('.fa-concierge-bell', 'wobble'); //icone das tentativas
}, 5000)

const verifyAcert  = (number = 0) => number == numSorteado ? success() : error(number);

const success = function(){
        querySelector('#msg h4', null, 'classList', 'alert-success');
        querySelector('#msg h4', 'Você ganhou... Parabéns!!!!');
        gamerOver();
}

const error = function(num){
    tentativas(--tentativasRestantes);
    if (tentativasRestantes == 0) {
        lost();
        return;
    }
    numErros.push(num);
    
    querySelector('#saidaDica h4', dica(num));
    querySelector('#saidaErro h4', numErros.join(", "));

}

const dica = function(num = 0){
    return num < numSorteado ? `Número maior que ${num}`: `Número menor que ${num}`;
}

const lost = function(){
    querySelector('#msg h4', null, 'classList', 'alert-danger');
    querySelector('#msg h4', 'Você Pedeu... Fim de jogo!!!!');
    
    gamerOver()
}

const gamerOver = function(){
    document.querySelectorAll('form small.text-muted').forEach(e => e.classList.toggle('d-none'));
    document.querySelectorAll('form hr.my-4').forEach(e => e.classList.add('d-none'));
    apostar.classList.toggle('d-none');
    jogar.classList.toggle('d-none');
}

apostar.addEventListener('click', (event) => {
    event.preventDefault();
    let num = document.querySelector("#numero").value;
    if(!num || !tentativasRestantes) return;
    if(num > 60){
        alert('Informe um número entre 0 e 60');
        return;
    }
    if(numErros.includes(num)) {
        querySelector('#saidaDica h4', 'Número já apostado!!');
        return;
    }
    verifyAcert(num);
});

jogar.addEventListener('click', function(){
    document.location.reload(true);
})