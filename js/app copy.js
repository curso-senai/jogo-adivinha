let tentativasRestantes = 6;
let numSorteado = 0;
let numErros = [];

const tentativas = (restante) => document.querySelector("#saidaChance h4").innerHTML = restante;

window.addEventListener('load', function () {
    numSorteado = Math.floor(Math.random() * (100 - 1) + 1);
    tentativas(tentativasRestantes);
});

setInterval(() => {
    animateCSS('#apostar', 'flipInX'); //botao
    animateCSS('.fa-heartbeat', 'swing'); //icone dos erros
    animateCSS('.fa-exclamation-circle', 'bounce'); //icone das chances
    animateCSS('.fa-concierge-bell', 'wobble'); //icone das tentativas
}, 5000)

function verifyAcert(number = 0){
    return number == numSorteado ? success() : error(number);
}

const success = function(){
        document.querySelector('#msg h4').classList.toggle("alert-success");
        document.querySelector('#msg h4').innerHTML = "Você ganhou... Parabéns!!!!";
        gamerOver();
}

const error = function(num){
    tentativas(--tentativasRestantes);
    if (tentativasRestantes == 0) {
        lost();
        return;
    }
    numErros.push(num);
    document.querySelector("#saidaDica h4").innerHTML = dica(num);
    document.querySelector("#saidaErro h4").innerHTML = numErros.join(", ");

}

const dica = function(num = 0){
    return num < numSorteado ? `Número maior que ${num}`: `Número menor que ${num}`;
}

const lost = function(){
    document.querySelector('#msg h4').classList.toggle("alert-danger");
    document.querySelector('#msg h4').innerHTML = "Você Pedeu... Fim de jogo!!!!";
    gamerOver()
}

const gamerOver = function(){
    document.querySelectorAll('form small.text-muted').forEach(e => e.classList.toggle('d-none'));
    document.querySelectorAll('form hr.my-4').forEach(e => e.classList.add('d-none'));
    apostar.classList.toggle('d-none');
    jogar.classList.toggle('d-none');
}

apostar.addEventListener('click', (event) => {
    event.preventDefault();
    let num = document.querySelector("#numero").value;
    if(!num || !tentativasRestantes) return;
    if(num > 100){
        alert('Informe um número entre 0 e 100');
        return;
    }
    verifyAcert(num);
});

jogar.addEventListener('click', function(){
    document.location.reload(true);
})